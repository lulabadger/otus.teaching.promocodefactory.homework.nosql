﻿using Microsoft.VisualBasic;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
         : IDbInitializer
    {
        private readonly IRepository<Employee> _emploerCollection;
        private readonly IRepository<Role> _roleCollections;
        private readonly IMongoDatabase _database; 
   
        public MongoDbInitializer(IMongoDatabase database, IRepository<Employee> emploerCollection, IRepository<Role> roleCollections)
        {
            _database = database;
            _emploerCollection = emploerCollection;
            _roleCollections = roleCollections;
        }
        private void DeleteCollections()
        {
            var collections = _database.ListCollectionNames().ToList();
            foreach (string c in collections)
            {
                _database.DropCollection(c);
            }
        }

        public void InitializeDb()
        {
            DeleteCollections();
            foreach (var item in FakeDataFactory.Roles)
            {
                _roleCollections.AddAsync(item);
            }

            foreach (var item in FakeDataFactory.Employees)
            {
                _emploerCollection.AddAsync(item);
            }
        }
    }
}
