﻿using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class MongoDataBaseSettings 
    {
        public string ConnectionStrings { get; set; }
        public string DatabaseName { get; set; }
        public string EmployeeCollectionName { get; set; }
        public string RoleCollectionName { get; set; }
    }
}
